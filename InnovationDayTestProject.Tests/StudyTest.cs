﻿using System;
using InnovationDayTestProject.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InnovationDayTestProject.Tests
{
    [TestClass]
    public class StudyTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var study = new Study();
            var studies = study.GetStudies();
            Assert.IsNotNull(studies);

        }

        [TestMethod]
        public void Study_ShouldReturnRecords()
        {
            var study = new Study();
            var studies = study.GetStudies();
            Assert.IsNotNull(studies);
            Assert.AreEqual(studies.Count, 4);
        }

        [TestMethod]
        public void Number()
        {
            Assert.AreEqual(1, 1);
        }

        [TestMethod]
        public void MinParticipants()
        {
            var study = new Study();
            var studies = study.GetStudies();
            Assert.AreEqual(studies[0].MinParticipants, 1000);
        }

        //[TestMethod]
        //public void Study_Desc()
        //{
        //    var study = new Study();
        //    var studies = study.GetStudies();
        //    Assert.IsNotNull(studies);
        //    Assert.AreEqual(studies[0].Name , studies[1].Name);
        //}
    }
}
