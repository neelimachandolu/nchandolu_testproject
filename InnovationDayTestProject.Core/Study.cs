﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnovationDayTestProject.Core
{
    public class Study
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MinParticipants { get; set; }

        public IList<Study> GetStudies()
        {
            return new List<Study>()
            {
                new Study{ Id =1, Name ="RAFT", Description="SMS Based Study", StartDate= Convert.ToDateTime("2020-11-01 13:10:00"), EndDate =Convert.ToDateTime("2021-11-01 13:10:00"),MinParticipants = 1000 },
                new Study{ Id =2, Name ="SS", Description="SS Study", StartDate= Convert.ToDateTime("2020-11-11 13:10:00"), EndDate =Convert.ToDateTime("2021-11-01 13:10:00"),MinParticipants = 1000 },
                new Study{ Id =3, Name ="Bushfire", Description="Bushfire Study", StartDate= Convert.ToDateTime("2020-11-21 13:10:00"), EndDate =Convert.ToDateTime("2021-12-01 13:10:00"),MinParticipants = 1000 },
                new Study{ Id =4, Name ="Pilot", Description="Pilot Study", StartDate= Convert.ToDateTime("2020-11-15 13:10:00"), EndDate =Convert.ToDateTime("2021-09-01 13:10:00"),MinParticipants = 1000 }
            };
        }
    }

    
}
